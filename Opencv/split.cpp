/* The piece of code below is an exercise that splits image components (r,g,b)
from an image, and puts them back.
Author: Roger
*/

// import opencv library
#include <opencv2/opencv.hpp>
#include<stdint.h>

using namespace std;
using namespace cv;

int main(int argv, char** argc){
    Mat original = imread("paul.jpg",CV_LOAD_IMAGE_GRAYSCALE);
    
    //split the color components in channels
   Mat splitChannels[3];
   split(original,splitChannels);
    
    imshow("B", splitChannels[0]);
    imshow("G", splitChannels[1]);
    imshow("R", splitChannels[1]);
splitChannels[2] = Mat::zeros(splitChannels[2].size(),CV_8UC1);

//merge
Mat output;
merge(splitChannels, 3, output);

waitKey();

}