/* The piece of code below is an exercise that work with windows in opencv.
It tries to work with moving window, and resizing windows given an image.
Author: Roger
*/

// import opencv library
#include <opencv2/opencv.hpp>
#include<stdint.h>

using namespace std;
using namespace cv;

int main(int argv, char** argc){
    Mat file1 = imread("paul.jpg",CV_LOAD_IMAGE_UNCHANGED);
    Mat file2 =imread("paul.jpg",CV_LOAD_IMAGE_GRAYSCALE);

    namedWindow("Color", CV_WINDOW_FREERATIO);
    namedWindow("Fixed", CV_WINDOW_AUTOSIZE);

    
    imshow("colored one", file1);
    imshow("gray one", file2);
    //sizing

    resizeWindow("Color", file1.col/2, file1.row/2);
    resizeWindow("Fixed",file2.col/2,file2.row/2);

    //moving window

    moveWindow("Color", 1600, 800);
    moveWindow("Fixed", 1600+ file1.cols +5, 800);


    waitKey();
}