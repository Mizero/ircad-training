/* The piece of code below is an exercise that plays with pixels in an image.
  It iterate through all the pixels of an image for colored and grayscale image.
Author: Roger
*/

// import opencv library
#include <opencv2/opencv.hpp>
#include<stdint.h>

using namespace std;
using namespace cv;

int main(int argv, char** argc){
    Mat original = imread("paul.jpg",CV_LOAD_IMAGE_GRAYSCALE);
    Mat modified =imread("paul.jpg",CV_LOAD_IMAGE_GRAYSCALE);

//iterate over the image
  //original.at<>(r,c);//template function
 for (int r=0; r < modified.rows; r++)
 {
  for (int c=0; c < modified.cols; c++)
  {
      modified.at<uint8_t>(r,c)= modified.at<uint8_t>(r,c)* 0.5f;
  }
 }   
// for colored images
Mat original1= imread("paul.jpg",CV_LOAD_IMAGE_COLOR);
    Mat modified1 =imread("paul.jpg",CV_LOAD_IMAGE_COLOR);

//iterate over the image
  //original.at<>(r,c);//template function
 for (int r=0; r < modified.rows; r++)
 {
  for (int c=0; c < modified.cols; c++)
  {
      modified.at<cv::Vec3b>(r,c)[1]= modified.at<cv::Vec3b>(r,c)[1]* 0;//use vector class. [1] is blue, ect
  }
 }   


    imshow("original", original);
    imshow("modified", modified);

waitKey();
}