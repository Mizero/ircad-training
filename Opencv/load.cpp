/* The piece of code below is an exercise that I wrote just to get started with opencv 
   It reads an image, displays it out, and saves it.
Author: Roger
*/

// import opencv library
#include <opencv2/opencv.hpp>
#include<stdint.h>

using namespace std;
using namespace cv;

int main(int argv, char** argc){
    Mat test = imread("Paul-Kagame.jpg", CV_LOAD_IMAGE_UNCHANGED);
     Mat testGray =imread("paul.jpg",CV_LOAD_IMAGE_GRAYSCALE);
    imshow("test",test);
    imwrite("output.jpg", testGray);
    waitKey();
}