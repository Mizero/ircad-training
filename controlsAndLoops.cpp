/* 
This piece of code is testing the understanding of controls and iterations. 
It uses only if and for loops.namespace.namespace. 
Author: Roger M
*/
#include<iostream>
using namespace std;

int main(){
    for (int i=1; i<20; i++){
if(i == 12){
    cout<<"twelve is ready \n";
}
else
   cout<<i <<endl;
   
    }
   return 0; 
}