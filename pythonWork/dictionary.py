phonebook = {"Roger": 783876825,"Peter":78366775348484, "Sem":783454883}
for name, number in phonebook.items():
    print("Phone number of %s is %d"%(name, number))

phonebook["Jake"] = 938273443
del phonebook["Sem"]

# testing code
if "Jake" in phonebook:
    print("Jake is listed in the phonebook.")
if "Jill" not in phonebook:
    print("Jill is not listed in the phonebook.")