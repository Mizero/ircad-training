#include<iostream>
using namespace std;

// create a class that calculate the area of a rectangle. 
// the class has two methods: one  to set the values, and another one to calculate the area.
class CRectangle {
 int x, y;
 public:
 void set_values (int,int);
 int area () {return (x*y);}
};
void CRectangle::set_values (int a, int b) {
 x = a;
 y = b;
}
int main () {
 CRectangle rect;
 rect.set_values (3,4);
 cout << "area: " << rect.area()<<"\n";
 return 0;
} 
