/* The piece of code below is an exercise of working with functions in cpp.
 These are just simple functions to perform artimethic operations.
 Author: Roger Mizero
*/
#include<iostream>
using namespace std;

// Function to add two numbers. it takes two parameters.
int add(int number1, int number2){
   
    int result = number1+ number2;

    return result;
}
//Function to substarct two numbers
int substract(int number1, int number2){
    
    int result = number1-number2;
    
    return result;
}

// Function to multiply two numbers
int multiply(int number1, int number2){
   
    int result = number1 * number2;

    return result;

}
// the main function. It calls all the functions and pass parameters. and displays results
int main(){
   int addition = add(100,200);
    int substraction= substract(300,150);
    int mult= multiply(20,4);

    cout<<"Addition result = "<<addition<<endl;
    cout<<"substration result = "<<substraction<<endl;
    cout<<"multiplication = "<<mult<< endl;

    return 0;

    }